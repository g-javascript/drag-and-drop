/******************************
 *                             Events fired on the draggable target
 ******************************/

 /**
 * The ondragstart event occurs when the user starts to drag an element or text selection.
 * the ondragstart attribute calls a drag(event) that specifies what data to be dragged
 */
function dragStart(ev) {
    // The dataTransfer.setData() method sets the data type (text )and the value (ev.target.id) of the dragged data
    ev.dataTransfer.setData("text", ev.target.id);
    document.getElementById("message").innerHTML = "ondragstart: the user starts to drag an element or text selection";
}

/**
 * The ondrag event occurs when an element or text selection is being dragged.
 */
function drag(ev) {
}

/**
 * The ondragend event occurs when the user has finished dragging an element or text selection.
 */
function dragEnd(ev) {
    document.getElementById("message").innerHTML = "ondragend: the user has finished dragging an element or text selection";
}

/******************************
 *                             Events fired on the drop target
 ******************************/

/**
 * The ondragenter event occurs when a draggable element or text selection enters a valid drop target.
 */
function dragEnter(ev) {
    document.getElementById("message").innerHTML = "ondragenter: a draggable element or text selection enters a valid drop target";
    if (ev.target.className == 'drop-target') {
        ev.target.style.border = "3px dotted green";
    }
}

/**
 * The ondragover event occurs when a draggable element or text selection is being dragged over a valid drop target.
 */
function dragOver(ev) {
    document.getElementById("message").innerHTML = "ondragover: a draggable element or text selection is being dragged over a valid drop target";
    ev.preventDefault();
}

/**
 * The ondragleave event occurs when a draggable element or text selection leaves a valid drop target.
 */
function dragLeave(ev) {
    document.getElementById("message").innerHTML = "ondragleave: a draggable element or text selection leaves a valid drop target";
    if (ev.target.className == 'drop-target') {
        ev.target.style.border = "";
    }
}

/**
 * The ondrop event occurs when a draggable element or text selection is dropped on a valid drop target.
 * By default, data/elements cannot be dropped in other elements. 
 * To allow a drop, we must prevent the default handling of the element.
 */
function drop(ev) {
    // Call preventDefault() to prevent the browser default handling of the data (default is open as link on drop)
    ev.preventDefault();
    // Get the dragged data with the dataTransfer.getData() method. This method will return any data that was set to the same type in the setData() method
    let data = ev.dataTransfer.getData("text");
    // Append the dragged element into the drop element
    ev.target.appendChild(document.getElementById(data));
}